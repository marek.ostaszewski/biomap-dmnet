# BIOMAP Disease Map network

The code for facilitating programmatic access and analysis of the BIOMAP Disease Map, hosted under [https://imi-biomap.elixir-luxembourg.org/minerva/](https://imi-biomap.elixir-luxembourg.org/minerva/).

The code uses the `minervar` R package, available at [https://gitlab.lcsb.uni.lu/minerva/minervar](https://gitlab.lcsb.uni.lu/minerva/minervar).

Files:
 - `net_access.R` download and format the network in a bipartite format, with identifiers of choice; generate a reduced version, removing 'reaction' nodes in the bipartite format, reconnecting the elements

 - `net_expand.R` expand the network by the contents of selected larger networks; currently by OmnipathDB [https://omnipathdb.org](https://omnipathdb.org)