### BIOMAP network expand using PathExpand and selected databases

### Load necessary libraries
library(purrr)
library(igraph)
library(dplyr)
library(OmnipathR)

### Code to run the PathExpand algorithm by Glaab et al 2013 
### [PMID: 21144022, doi: 10.1186/1471-2105-11-597]
source("pathexpand_base.R")

### Load the network constructed from the BIOMAP Disease Map
net_to_expand <- read.table("reduced_network.tsv", sep = "\t", header = T)

generate_omnipath_network <- function() {
  ### Network to use for expansion. Here we rely on the OmnipathDB
  network_src <- OmnipathR::import_all_interactions()
  
  ### Take only directed interactions, add "UNIPROT:" prefix to match our BIOMAP network
  network_file <- dplyr::filter(network_src, consensus_direction == 1 & n_references > 0) %>%
    dplyr::select(source, target) %>%
    dplyr::mutate(source = paste0("UNIPROT:",source), target = paste0("UNIPROT:",target))
  
  write.table(network_file, file = "omnipath_network.tsv", sep = "\t",
              col.names = TRUE, row.names = FALSE, quote = FALSE)
}

#network_file = "hppifinal2019.txt"

expand_network <- function(core_network, big_network_path) {
  core_list <- grep("^UNIPROT", unlist(core_network), value = T)
  big_network <- read.table(big_network_path, sep = "\t", header = TRUE)
  colnames(big_network) <- c("from", "to")
  ### Expand with 'pathexpand', setting the minimum pathway connections threshold to 2
  results <- pathexpand(core_list, big_network_path, direct_thresh = 2)
  ### Create a lookup table to identify edges to add
  results_lookup <- igraph::as_edgelist(results$extended_subnetwork) %>%
    c(paste0(.[,1],"-",.[,2]), paste0(.[,2],"-",.[,1]))
  to_add <- big_network[paste0(big_network$from,"-",big_network$to) %in% results_lookup,]
  return(list(expanded_network = rbind(core_network,to_add), new_nodes = results$added_genes))
}

plus_op <- expand_network(net_to_expand, "omnipath_network.tsv")
message("First iteration: ", nrow(plus_op$expanded_network), 
        " with ", length(plus_op$new_nodes), " new nodes")

while(length(plus_op$new_nodes) > 0) {
  plus_op <- expand_network(plus_op$expanded_network, "omnipath_network.tsv")
  message("Next iteration: ", nrow(plus_op$expanded_network), 
          " with ", length(plus_op$new_nodes), " new nodes")
}

######
### Write down the reduced network
######

write.table(plus_op$expanded_network, file = "reduced_expanded_network.tsv", sep = "\t",
            col.names = TRUE, row.names = FALSE, quote = FALSE)